package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;


@WebFilter(urlPatterns = {"/setting", "/edit", })
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();

		//以下で使用するHttpServletRequest、HttpServletResponseで使えるgetSession、sendRedirectを使えるようにするための型変換
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		User loginUser = (User)httpRequest.getSession().getAttribute("loginUser");
        if(loginUser != null){
            //セッションにユーザの情報があれば、通常どおりの遷移
            chain.doFilter(request, response);

        }else{
            //セッションにユーザの情報がなければ、エラーメッセージを表示してログイン画面へ飛ばす
        	List<String> errorMessages = new ArrayList<String>();
            errorMessages.add("ログインしてください");
            session.setAttribute("errorMessages", errorMessages);
        	httpResponse.sendRedirect("./login");
        }

	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}