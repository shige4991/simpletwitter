package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        //URLのつぶやきのIDを数字以外に変更、URLのつぶやきのIDを削除したときはエラー
        //つまりIDが数字かつBlankでない場合にメッセージを表示
        Message message = null;
        String id = request.getParameter("message_id");
        if(id.matches("^[0-9]+$") && !StringUtils.isBlank(id)){
			int messageId = Integer.parseInt(id);
			message = new MessageService().select(messageId);
        }

        //URLのつぶやきのIDを存在しないつぶやきのIDに変更
        //直上のif文にあてはならない、つまりIDが数字ではないまたはBlankの場合
        //直上のif文が実行されないことでmessage = new MessageService().select(messageId);は実行されず、messageにはnullが入ったまま
        if(message == null) {
        	List<String> errorMessages = new ArrayList<String>();
        	errorMessages.add("不正なパラメータが入力されました");
        	request.setAttribute("errorMessages", errorMessages);
        	request.getRequestDispatcher("./").forward(request, response);
        	return;
        }

        request.setAttribute("message", message);
        request.getRequestDispatcher("/edit.jsp").forward(request, response);
	}

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

    	Message message = new Message();
    	message.setId(Integer.parseInt(request.getParameter("id")));
    	message.setText(request.getParameter("text"));

        String text = request.getParameter("text");
        if (!isValid(text, errorMessages)) {
        	session.setAttribute("errorMessages",errorMessages);
        	request.setAttribute("message", message);
        	request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }

    	new MessageService().update(message);
		response.sendRedirect("./");

    }

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}